# Paraview Tutorial


## Installation

Download and install [Paraview](https://www.paraview.org/download/)

## Data generation

We've provide some data in `bubble_life_3d` and `four_bubbles_2d`.

Here's the data type we used:

- Particle: `.vtp`
- Mesh: `.ply`/`.vtu`
- Field: `.vts`

To output the VTK files (`vtp`/`vtu`/`vts`), you may need to use VTK-related libraries.

### C++

In C++, the original [VTK system](https://github.com/Kitware/VTK) is quite large. However, there are simplified repositories available for VTK data input and output:

- [LeanVTK](https://github.com/mmorse1217/lean-vtk)(untest)
- [SimpleVTK](https://github.com/hsimyu/SimpleVTK)(untest)

### Python

If you are using Taichi, the VTK-related libraries are already installed. 
Taichi provides some simple VTK writers in [vtk.py](https://github.com/taichi-dev/taichi/blob/a992f22ee34f89cd86fbd28da07d84be4c3161cf/python/taichi/tools/vtk.py#L4)

Otherwise, please install `pyevtk` for VTK IO.

## Basic pipeline for data visualization

1. Open the files by `File->Open`.
2. Locate and select your file within `Pipeline Browser` panel.
3. Update the properties or apply filters by `Filters->[Some filter]` to visualize the result.
4. Observe the visualization results in the `Render View` panel.
5. Save the visualization results by `File->Save Screenshot` or `File->Save Animation`. (Note: If saving as an animation, you can convert the animation frames into video format using FFMPEG)
6. Save the current state of visualization through `File->Save State`, so you can restore the state by `File->Load State` next time.


### Load simulation sequence

If your files are named with continuous frame numbers, you can load them as a sequence:

![](./doc/open_sequence.png)

1. Open `Animation view` panel by checking `View->Animation View`.
2. Change the mode to `Snap To Timesteps` within `Animation view` panel.
3. To browse through the results of different frames, adjust the time in `Animation view` panel.

![](./doc/animation_view.png)


## Visualization

### SpreadSheet View

You can display the file's underlying data in a tabular format with `SpreadSheet View`.

![](./doc/open_spread_sheet_view.png)

### Color map editor

In `Properties` panel, you can edit the color of the visualization within `Coloring` group.

![](./doc/coloring.png)

1. The attribute for coloring
2. Open `Color Map Editor`
3. Use separate color map
4. Rescale to data range
5. Rescale to custom range
6. Rescale to data range over all timesteps
7. Choose preset
8. Show/hide color legend

### Background

You can change the color of the background with `Load a color palette`

![](doc/set_backgroud.png)

## Visualization of different geometries

### Point/Particle

The point is loaded from a `.vtp` file.

1. Load the file.
2. Select the file in `Pipeline Browser` panel.
3. Apply `Glyph` filter by `Filters->Search->Input glyph`.
4. Select `Glyph` filter in `Pipeline Browser` panel and click `Apply` for visualization.
5. Change interpret type in `Properties->Glyph Source->Glyph Type`. (`Arrow`/`Sphere`/`2D glyph`).
6. Change visualization type in `Properties->Display->Representaion`. (`Surface`/`Point`/`Outline`/`Wireframe`)
7. Set `Scale Factor` in `Properties->Scale`  (length for `Arrow`, radius for `Sphere`)
8. Set the number of visualized particle in `Properties->Masking->Glyph Mode`. (`All points`/`Every Nth Point`/`Uniform Spatial Distribution`).
9. Set the color in `Properties->Coloring`.
10. Set the opacity, point size (only for `Representation/Point`?) and line width (only for `Representation/Wireframe`?)
11. Click `Apply` to apply all change.

### Mesh

The mesh is loaded from a `.ply`/`.vtu` file.

1. Load the file.
2. Select the file in `Pipeline Browser` panel.
3. Click `Apply` for visualization.
4. Change visualization type in `Properties->Display->Representaion`. (`Surface`/`Point`/`Outline`/`Wireframe`)
5. Set the color in `Properties->Coloring`.
6. Set the opacity, point size (only for `Representation/Point`?) and line width (only for `Representation/Wireframe`?)
7. Click `Apply` to apply all change.

### Scalar field on grid

The scalar field is loaded from a `.vts` file of `StructuredGrid` type.

1. Load the file.
2. Select the file in `Pipeline Browser` panel.
3. Click `Apply` for visualization.
4. Change visualization type in `Properties->Display->Representaion`. (`Surface`)
5. Set the attribute for coloring in `Properties->Coloring`.
6. Open `Color Map Editor`.


#### Smooth color map:

1. Uncheck `Interpret Values As Categories` in `Color Map Editor`.

#### Discrete color map:

1. Check `Interpret Values As Categories` in `Color Map Editor`
2. Set annotations in `Annotations`

#### Contour:

1. Apply `Contour` filter
2. Add isosurfaces in `Properties->Isosurfaces`
3. Set the color in `Properties->Coloring`.
4. Set the opacity, point size (only for `Representation/Point`?) and line width (only for `Representation/Wireframe`?)

### Vector field on grid

The scalar field is loaded from a `.vts` file of `StructuredGrid` type.

1. Load the file.
2. Select the file in `Pipeline Browser` panel.
3. Hide it by unchecking the eye icon in `Pipeline Browser`.
4. Apply `Glyph` filter by `Filters->Search->Input glyph`.
5. Select `Glyph` filter in `Pipeline Browser` panel and click `Apply` for visualization.
6. Change interpret type to `Arrow` in `Properties->Glyph Source->Glyph Type`.
7. Set `Properties->Orientation->Orientation Array` to the vector attribute of your file.
8. Set `Properties->Scale->Scale Array` to the vector attribute of your file.
9. Adjust `Scale Factor` in `Properties->Scale`.
10. Set the number of visualized particle in `Properties->Masking->Glyph Mode`. (`All points`/`Every Nth Point`/`Uniform Spatial Distribution`).
11. Set the color in `Properties->Coloring`.
12. Set the opacity, point size (only for `Representation/Point`?) and line width (only for `Representation/Wireframe`?)
13. Click `Apply` to apply all change.

## Gallery

### Four bubbles 2D

#### Wireframe of surface mesh + Outline of an arbitrary field

![](doc/surface_rendered.0000.png)

#### Wireframe of surface mesh + Indicator map colored by a discrete color map

![](doc/indicator_rendered.0000.png)

#### Indicator map colored by a discrete color map + colored interface particles

![](doc/particle_rendered.0000.png)


#### Contour of the level set

![](doc/contour_rendered.0000.png)

### Four bubbles 3D

#### Colored surface mesh

![](doc/regions.0000.png)
![](doc/regions.0250.png)

### Bubble cluster 2D

#### Wireframe of surface mesh + Indicator map colored by a discrete color map

![](doc/color_inidicator.2000.png)

#### Wireframe of surface mesh + Blue triangle mesh for liquid (output by c++ code)

![](doc/two_phase_indicator.2000.png)